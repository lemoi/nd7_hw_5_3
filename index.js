function add(numbers) {

    if (/.*\n$/.test(numbers)) {
        throw new Error('Ошибочное значение');
    }

    let res = /^\/\/(<.+>)\n(.*)/g.exec(numbers);
    if (res !== null) {
        numbers = res[2];
        res[1].slice(1, -1).split(/></).forEach(currentValue => numbers = numbers.split(currentValue).join());
    }

    let negativeNumbers = [];

    let result = numbers
        .replace(/\n\b/, ',')
        .split(',')
        .reduce((accumulator, currentValue) => {
                let number = Number(currentValue);
                if (number < 0) {
                    negativeNumbers.push(number);
                }
                return number > 1000 ? accumulator : accumulator + number;
            }, 0);

    if (negativeNumbers.length) {
        throw new Error('Отрицательные числа не допустимы. ' + negativeNumbers.join());
    }

    return result;

}

module.exports = add;